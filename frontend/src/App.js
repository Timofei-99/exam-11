import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import AppToolbar from "./Components/UI/AppToolBar/AppToolBar";
import {Route, Switch} from "react-router";
import Register from "./Containers/Register/Register";
import Login from "./Containers/Login/Login";
import {Grid} from "@material-ui/core";
import Items from "./Containers/Items/Items";
import AddNewItem from "./Containers/AddNewItem/AddNewItem";
import SoloItem from "./Containers/SoloItem/SoloItem";
import ByCategory from "./Containers/ByCategory/ByCategory";

const App = () => {
    return (
        <>
            <CssBaseline/>
            <header>
                <AppToolbar/>
            </header>
            <main>
                <Container maxWidth="xl">
                    <Grid container justifyContent="space-around">
                        <Switch>
                            <Route path="/" exact component={Items}/>
                            <Route path="/login" component={Login}/>
                            <Route path="/register" component={Register}/>
                            <Route path="/new-item" component={AddNewItem}/>
                            <Route path="/items/:id" component={SoloItem}/>
                            <Route path="/categories/:id" component={ByCategory}/>
                        </Switch>
                    </Grid>
                </Container>
            </main>
        </>
    );
};

export default App;
