import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import {Router} from "react-router-dom";
import {createTheme, MuiThemeProvider} from "@material-ui/core";
import {Provider} from "react-redux";
import store from "./store/configureStore";
import history from "./history";
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer} from "react-toastify";

const theme = createTheme({
    props: {
        MuiTextField: {
            variant: "outlined",
            fullWidth: true,
        },
    },
});

const app = (
    <Provider store={store}>
        <Router history={history}>
            <MuiThemeProvider theme={theme}>
                <ToastContainer/>
                <App/>
            </MuiThemeProvider>
        </Router>
    </Provider>
);

ReactDOM.render(app, document.getElementById("root"));
