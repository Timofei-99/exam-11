import axiosShop from "../../axiosShop";

export const FETCH_CATEGORIES_REQUEST = "FETCH_CATEGORIES_REQUEST";
export const FETCH_CATEGORIES_SUCCESS = "FETCH_CATEGORIES_SUCCESS";
export const FETCH_CATEGORIES_FAILURE = "FETCH_CATEGORIES_FAILURE";

export const fetchCategoriesRequest = () => ({type: FETCH_CATEGORIES_REQUEST});
export const fetchCategoriesSuccess = (categories) => ({type: FETCH_CATEGORIES_SUCCESS, categories});
export const fetchCategoriesFailure = (error) => ({type: FETCH_CATEGORIES_FAILURE, error});

export const fetchCategories = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchCategoriesRequest());
            const response = await axiosShop.get("/categories");
            dispatch(fetchCategoriesSuccess(response.data));
        } catch (e) {
            dispatch(fetchCategoriesFailure(e));
        }
    };
};
