import axiosShop from "../../axiosShop";
import {historyPush} from "./historyActions";
import {toast} from "react-toastify";

export const FETCH_ITEMS_REQUEST = "FETCH_ITEMS_REQUEST";
export const FETCH_ITEMS_SUCCESS = "FETCH_ITEMS_SUCCESS";
export const FETCH_ITEMS_FAILURE = "FETCH_ITEMS_FAILURE";


export const FETCH_ITEM_REQUEST = "FETCH_ITEM_REQUEST";
export const FETCH_ITEM_SUCCESS = "FETCH_ITEM_SUCCESS";
export const FETCH_ITEM_FAILURE = "FETCH_ITEM_FAILURE";

export const FETCH_ITEMSCATEGORY_REQUEST = "FETCH_ITEMSCATEGORY_REQUEST";
export const FETCH_ITEMSCATEGORY_SUCCESS = "FETCH_ITEMSCATEGORY_SUCCESS";
export const FETCH_ITEMSCATEGORY_FAILURE = "FETCH_ITEMSCATEGORY_FAILURE";

export const DELETE_ITEM_REQUEST = "DELETE_ITEM_REQUEST";
export const DELETE_ITEM_SUCCESS = "DELETE_ITEM_SUCCESS";
export const DELETE_ITEM_FAILURE = "DELETE_ITEM_FAILURE";

export const fetchItemsRequest = () => ({type: FETCH_ITEMS_REQUEST});
export const fetchItemsSuccess = (items) => ({type: FETCH_ITEMS_SUCCESS, items});
export const fetchItemsFailure = (error) => ({type: FETCH_ITEMS_FAILURE, error});

export const postItemRequest = () => ({type: FETCH_ITEMS_REQUEST});
export const postItemSuccess = () => ({type: FETCH_ITEMS_SUCCESS});
export const postItemFailure = (error) => ({type: FETCH_ITEMS_FAILURE, error});

export const fetchItemRequest = () => ({type: FETCH_ITEM_REQUEST});
export const fetchItemSucess = (item) => ({type: FETCH_ITEM_SUCCESS, item});
export const fetchItemFailure = (error) => ({type: FETCH_ITEM_FAILURE, error});

export const fetchItemsByCategoryRequest = () => ({
    type: FETCH_ITEMSCATEGORY_REQUEST,
});

export const fetchItemsByCategorySuccess = (itemsCategory) => ({
    type: FETCH_ITEMSCATEGORY_SUCCESS,
    itemsCategory,
});

export const fetchItemsByCategoryFailure = (error) => ({
    type: FETCH_ITEMSCATEGORY_FAILURE,
    error,
});

export const deleteItemRequest = () => ({type: DELETE_ITEM_REQUEST});
export const deleteItemSuccess = () => ({type: DELETE_ITEM_SUCCESS});
export const deleteItemFailure = (error) => ({type: DELETE_ITEM_FAILURE, error});

export const fetchItems = () => {
    return async (dispacth) => {
        try {
            dispacth(fetchItemsRequest());
            const response = await axiosShop.get("/items");
            dispacth(fetchItemsSuccess(response.data));
        } catch (e) {
            dispacth(fetchItemsFailure(e));
        }
    };
};

export const postItems = (itemData) => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        if (token === null) {
            dispatch(historyPush("/login"));
        }
        try {
            dispatch(postItemRequest());
            await axiosShop.post("/items", itemData, {headers: {Authorization: token}});
            dispatch(postItemSuccess());
            dispatch(historyPush("/"));
            toast.success("Create Item successful");
        } catch (e) {
            dispatch(postItemFailure(e));
            toast.error("Fields filled in incorrectly");
        }
    };
};

export const fetchItem = (id) => {
    return async (dispatch) => {
        try {
            dispatch(fetchItemRequest());
            const response = await axiosShop.get("/items/" + id);
            dispatch(fetchItemSucess(response.data));
        } catch (e) {
            dispatch(fetchItemFailure(e));
        }
    };
};

export const fetchItemsByCategory = (id) => {
    return async (dispatch) => {
        try {
            dispatch(fetchItemsByCategoryRequest());
            const response = await axiosShop.get("/items?category=" + id);
            dispatch(fetchItemsByCategorySuccess(response.data));
        } catch (e) {
            dispatch(fetchItemsByCategoryFailure(e));
        }
    };
};

export const deleteItem = (id) => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        if (token === null) {
            toast.warning("You are not a seller");
        }
        try {
            dispatch(deleteItemRequest());
            await axiosShop.delete("/items/" + id, {headers: {Authorization: token}});
            dispatch(deleteItemSuccess());
            dispatch(historyPush("/"));
            toast.success("Item deleted");
        } catch (e) {
            dispatch(deleteItemFailure(e));
            toast.error("An Error Occurred While Deleting")
        }
    };
};
