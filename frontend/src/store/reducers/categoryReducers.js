import {
    FETCH_CATEGORIES_FAILURE,
    FETCH_CATEGORIES_REQUEST,
    FETCH_CATEGORIES_SUCCESS,
} from "../actions/categoryActions";

const initialState = {
    categories: [],
    categoryLoading: false,
    categoryError: false,
};

const categoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CATEGORIES_REQUEST:
            return {...state, categoryLoading: true};
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, categoryLoading: false, categories: action.categories};
        case FETCH_CATEGORIES_FAILURE:
            return {...state, categoryLoading: false, categoryError: action.error};
        default:
            return state;
    }
};
export default categoryReducer;
