import {Card, CardContent, CardMedia, Grid, makeStyles} from "@material-ui/core";
import React from "react";
import {Link} from "react-router-dom";
import imageNotAvailable from "../../assets/images/imageNotAvailable.png";
import {apiURL} from "../../config";

const useStyles = makeStyles({
    card: {
        height: "auto",
        width: "250px",
    },
    cardLink: {
        textDecoration: "none",
        width: "30%",
        margin: "10px",
    },
    media: {
        height: 0,
        paddingTop: "56.25%",
    },
    cardTitle: {
        fontSize: "20px",
        marginLeft: "20px",
    },
});

const ItemsItem = ({id, price, title, image}) => {
    const classes = useStyles();

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + "/" + image;
    }

    return (
        <Grid item container component={Link} to={"/items/" + id} className={classes.cardLink}>
            <Card className={classes.card}>
                <CardMedia image={cardImage} title={title} className={classes.media}/>
                <p className={classes.cardTitle}>{title}</p>
                <CardContent>
                    <p>Price {price} KGS</p>
                </CardContent>
            </Card>
        </Grid>
    );
};

export default ItemsItem;
