import {Grid, Typography} from "@material-ui/core";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {fetchCategories} from "../../store/actions/categoryActions";

const Category = () => {
    const dispatch = useDispatch();

    const categories = useSelector((state) => state.category.categories);

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);

    return (
        <Grid container spacing={2} direction="column" style={{margin: "50px 0 0"}}>
            <Typography variant="h5">Categories</Typography>
            <Grid item>
                <Link to="/">All items</Link>
            </Grid>
            {categories.map((category) => (
                <Grid item key={category._id}>
                    <Link to={"/categories/" + category._id}>{category.title}</Link>
                </Grid>
            ))}
        </Grid>
    );
};

export default Category;
