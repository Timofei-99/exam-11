import {Button, Grid, Typography} from "@material-ui/core";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {apiURL} from "../../config";
import {deleteItem, fetchItem} from "../../store/actions/itemsActions";
import imageNotAvailable from "../../assets/images/imageNotAvailable.png";

const SoloItem = (props) => {
    const dispatch = useDispatch();

    const item = useSelector((state) => state.items.item);
    const user = useSelector((state) => state.users.user);

    useEffect(() => {
        dispatch(fetchItem(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    return (
        <>
            {item !== null ? (
                <Grid container direction="column" style={{margin: "25px"}}>
                    <Typography variant="h3">{item.title}</Typography>
                    <p>Category: {item.category.title}</p>

                    {item.image ? (
                        <img
                            src={apiURL + "/" + item.image}
                            alt={item.title}
                            style={{width: "600px", margin: "20px 0"}}
                        />
                    ) : (
                        <img src={imageNotAvailable} alt="None Items" style={{width: "600px", margin: "20px 0"}}/>
                    )}
                    <Typography variant="h6">{item.description}</Typography>
                    <p>Price: {item.price} KGS</p>
                    <p style={{fontSize: "20px"}}>
                        {item.seller.displayName} <i>Phone: {item.seller.phone} </i>
                    </p>

                    {user ? (
                        <Grid item>
                            {user._id !== item.seller._id ? null : (
                                <Button onClick={() => dispatch(deleteItem(item._id))} variant="outlined">
                                    Delete item
                                </Button>
                            )}
                        </Grid>
                    ) : null}
                </Grid>
            ) : null}
        </>
    );
};

export default SoloItem;
