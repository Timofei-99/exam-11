import {Grid, Typography} from "@material-ui/core";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchItemsByCategory} from "../../store/actions/itemsActions";
import Category from "../Category/Category";
import ItemsItem from "../Items/ItemsItem";

const ByCategory = ({match}) => {
    const dispatch = useDispatch();

    const items = useSelector((state) => state.items.itemsCategory);

    useEffect(() => {
        dispatch(fetchItemsByCategory(match.params.id));
    }, [dispatch, match.params.id]);

    return (
        <Grid container justifyContent="space-around">
            <Grid item style={{width: "30%"}}>
                <Category/>
            </Grid>
            <Grid item style={{width: "70%"}}>
                {items.length > 0 ? (
                    <Typography variant="h3">{items[0].category.title}</Typography>
                ) : (
                    <Typography variant="h4">Product list is empty</Typography>
                )}
                <Grid item container style={{display: "flex", flexWrap: "wrap"}}>
                    {items
                        ? items.map((item) => (
                            <ItemsItem
                                key={item._id}
                                id={item._id}
                                title={item.title}
                                price={item.price}
                                image={item.image}
                            />
                        ))
                        : null}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default ByCategory;
