const mongoose = require("mongoose");
const config = require("./config");
const {nanoid} = require("nanoid");
const User = require("./models/User");
const Category = require("./models/Category");
const Item = require("./models/Item");

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (let coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [computers, appliances, telephones, different] = await Category.create(
        {
            title: "Computers",
        },
        {
            title: "Appliances",
        },
        {
            title: "Telephones",
        },
        {
            title: "Different",
        }
    );

    const [user, user1] = await User.create(
        {
            username: "user",
            password: "12345",
            token: nanoid(),
            phone: "+996555021031",
            displayName: "Tima",
        },
        {
            username: "user2",
            password: "12345",
            token: nanoid(),
            phone: "+996555021031",
            displayName: "Tima2",
        }
    );

    await Item.create(
        {
            title: "Видеокарта GTX 1050ti",
            price: 10000,
            category: computers,
            seller: user,
            image: "fixtures/gtx-1050.jpeg",
            description: "4gb 256-bit",
        },
        {
            title: "Стол",
            price: 5000,
            category: appliances,
            seller: user1,
            image: "fixtures/stol.jpeg",
            description: "120x75x60",
        },
        {
            title: "Iphone 11",
            price: 13000,
            category: telephones,
            seller: user1,
            image: "fixtures/iphone11.jpeg",
            description: "17x19x2",
        },
        {
            title: "Наушники",
            price: 1000,
            category: different,
            seller: user,
            image: "fixtures/airpods.jpeg",
            description: "Лучшие Наушники ",
        }
    );

    await mongoose.connection.close();
};

run().catch(console.error);
